image: docker:19.03.1

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""
  DOCKER_HOST: tcp://docker:2375
  MOUNT_POINT: /builds/$CI_PROJECT_PATH/mnt
  CONTAINER_IMAGE_FE: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/fe:$CI_COMMIT_SHA
  CONTAINER_IMAGE_DB: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG/db:$CI_COMMIT_SHA

services:
  - docker:dind

include:
  - template: Jobs/Code-Quality.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Quality.gitlab-ci.yml
  - template: Jobs/Code-Intelligence.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Code-Intelligence.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - template: Coverage-Fuzzing.gitlab-ci.yml


stages:
    - build 
    - test
    - fuzz
    - code-coverage
    - report
    - review 
    - incremental rollout 10%
    - incremental rollout 25%
    - incremental rollout 50%
    - incremental rollout 100%
    - staging
    - canary
    - deploy
    - performance

build-images:
  stage: build
  script:
    - sleep 5
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CONTAINER_IMAGE_FE -f Dockerfile.fe .
    - docker push $CONTAINER_IMAGE_FE
  tags:
     - docker
     - gce

build-images-db:
  stage: build
  script:
    - sleep 5
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CONTAINER_IMAGE_DB -f Dockerfile.db .
    - docker push $CONTAINER_IMAGE_DB
  tags:
     - docker
     - gce
  
test-code:
    stage: test
    image: python
    script:
        - pip install pytest pytest-cov
        - coverage run -m pytest 
        - echo "generating report"    
        - coverage report
        - coverage xml

    artifacts:
        reports:
            cobertura: coverage.xml

my_fuzz_target:
  image: python:latest
  extends: .fuzz_base
  script:
    #- pip install --extra-index-url https://gitlab.com/api/v4/projects/19904939/packages/pypi/simple pythonfuzz
    - pip install pythonfuzz
    - ./gitlab-cov-fuzz run --engine pythonfuzz -- fuzz.py


            
analyze-test-code-coverage:
    stage: code-coverage
    image: tiangolo/uwsgi-nginx-flask:python3.6-alpine3.7 
    script:
        - echo " analyzing test code coverage"
        - pip install -r requirements.txt
        - pip install coverage
        - coverage run sql_queries.py
        - coverage report
        - coverage xml
        
    artifacts:
        reports:
            cobertura: coverage.xml

review-application:
    stage: review
    image: lachlanevenson/k8s-kubectl:latest
    needs: ["analyze-test-code-coverage"]
    environment:
     name: review/$CI_COMMIT_REF_NAME
     url: https://$CI_ENVIRONMENT_SLUG.34.91.210.148.nip.io
    
    script:
      - kubectl version
      - cd k8s/
      - sed -i  "s|{CONTAINER_IMAGE_FE}|$CONTAINER_IMAGE_FE|" k8s-server-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-server-deploy.yaml
      - echo $CI_PROJECT_PATH_SLUG
      - sed -i "s|{CI_PROJECT_PATH_SLUG}|$CI_PROJECT_PATH_SLUG|" k8s-server-deploy.yaml
      - cat k8s-server-deploy.yaml
      - sed -i  "s|{CONTAINER_IMAGE_DB}|$CONTAINER_IMAGE_DB|" k8s-db-deploy.yaml
      - cat k8s-db-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-ext-svc.yaml
      - kubectl apply -f k8s-server-deploy.yaml && kubectl apply -f k8s-ext-svc.yaml
      - kubectl apply -f k8s-db-svc.yaml && kubectl apply -f k8s-db-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-ingress.yaml
      - cat k8s-ingress.yaml
      - kubectl apply -f k8s-ingress.yaml
      - kubectl get deployments,services
    only:
     - branches
    except:
     - master 
    tags:
     - devops

staging:
   stage: staging
   image: lachlanevenson/k8s-kubectl:latest
   environment:
     name: staging
     url: https://$CI_ENVIRONMENT_SLUG.34.91.210.148.nip.io
   script:
      - cd k8s/
      - sed -i  "s|{CONTAINER_IMAGE_FE}|$CONTAINER_IMAGE_FE|" k8s-server-staging.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-server-staging.yaml
      - sed -i "s|{CI_PROJECT_PATH_SLUG}|$CI_PROJECT_PATH_SLUG|" k8s-server-staging.yaml
      - sed -i  "s|{CONTAINER_IMAGE_DB}|$CONTAINER_IMAGE_DB|" k8s-db-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-ext-svc.yaml
      - kubectl apply -f k8s-server-staging.yaml && kubectl apply -f k8s-ext-svc.yaml
      - kubectl apply -f k8s-db-svc.yaml && kubectl apply -f k8s-db-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-ingress.yaml
      - kubectl apply -f k8s-ingress.yaml
      - kubectl get deployments,services
   only:
     - master
   tags:
     - devops

# canary:
#    stage: canary
#    image: lachlanevenson/k8s-kubectl:latest
#    environment:
#      name: production
#      url: https://$CI_ENVIRONMENT_SLUG.34.91.210.148.nip.io
#    script:
#       - cd k8s/
#       - sed -i  "s|{CONTAINER_IMAGE_FE}|$CONTAINER_IMAGE_FE|" k8s-server-deploy-canary.yaml
#       - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-server-deploy-canary.yaml
#       - sed -i "s|{CI_PROJECT_PATH_SLUG}|$CI_PROJECT_PATH_SLUG|" k8s-server-deploy-canary.yaml
#       - sed -i  "s|{CONTAINER_IMAGE_DB}|$CONTAINER_IMAGE_DB|" k8s-db-deploy.yaml
#       - kubectl apply -f k8s-server-deploy-canary.yaml && kubectl apply -f k8s-ext-svc.yaml
#       - kubectl apply -f k8s-db-svc.yaml && kubectl apply -f k8s-db-deploy.yaml
#       - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-ingress.yaml
#       - kubectl apply -f k8s-ingress.yaml
#       - kubectl get deployments,services
#    only:
#      - master
#    tags:
#      - devops


production-deployment:
   stage: deploy
   image: lachlanevenson/k8s-kubectl:latest
   needs: ["staging"]
   environment:
     name: production
     url: https://$CI_ENVIRONMENT_SLUG.34.91.210.148.nip.io
   script:
      - cd k8s/
      - sed -i  "s|{CONTAINER_IMAGE_FE}|$CONTAINER_IMAGE_FE|" k8s-server-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-server-deploy.yaml
      - sed -i "s|{CI_PROJECT_PATH_SLUG}|$CI_PROJECT_PATH_SLUG|" k8s-server-deploy.yaml
      - sed -i  "s|{CONTAINER_IMAGE_FE}|$CONTAINER_IMAGE_FE|" k8s-server-deploy-canary.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-server-deploy-canary.yaml
      - sed -i "s|{CI_PROJECT_PATH_SLUG}|$CI_PROJECT_PATH_SLUG|" k8s-server-deploy-canary.yaml
      - sed -i  "s|{CONTAINER_IMAGE_DB}|$CONTAINER_IMAGE_DB|" k8s-db-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-ext-svc.yaml
      - kubectl apply -f k8s-server-deploy.yaml && kubectl apply -f k8s-ext-svc.yaml
      - kubectl apply -f k8s-server-deploy-canary.yaml
      - kubectl apply -f k8s-db-svc.yaml && kubectl apply -f k8s-db-deploy.yaml
      - sed -i "s|{CI_ENVIRONMENT_SLUG}|$CI_ENVIRONMENT_SLUG|" k8s-ingress.yaml
      - kubectl apply -f k8s-ingress.yaml
      - kubectl get deployments,services
   only:
     - master
   tags:
     - devops

performance:
  stage: performance
  needs: ["production-deployment"]
  script:
    - echo "testing performance"
  only:
    - master

